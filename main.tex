\documentclass[a4,center,fleqn]{NAR}

\usepackage{url}
\usepackage[hidelinks]{hyperref}
\usepackage[version=4]{mhchem}
\usepackage{xr}
\usepackage[dvipsnames]{xcolor}
\usepackage{newpxtext,newpxmath}
\usepackage{color,soul}
\sethlcolor{red}


\externaldocument[A-]{appendix}


% Enter dates of publication
\copyrightyear{2022}
\pubdate{1 January 2022}
\pubyear{2022}
\jvolume{37}
\jissue{12}


\newcommand{\Cmat}{\mathbf{C}}
\newcommand{\Lmat}{\mathbf{L}}
\newcommand{\Xmat}{\mathbf{X}}
\newcommand{\Smat}{\mathbf{S}}
\newcommand{\Qmat}{\mathbf{Q}}
\newcommand{\Gmat}{\mathbf{G}}
\newcommand{\Gammamat}{\mathbf{\Gamma}}
\newcommand{\Zeromat}{\mathbf{0}}
\newcommand{\Eyemat}{\mathbf{I}}
\newcommand{\PRmat}[1]{\mathbf{P}_{\mathcal{R}\left(\mathbf{#1}\right)}}
\newcommand{\PNmat}[1]{\mathbf{P}_{\mathcal{N}\left(\mathbf{#1}\right)}}
\newcommand{\Covmat}[1]{\mathbf{\Sigma}({\mathbf{#1}})}


\begin{document}

    \title{eQuilibrator 3.0 -- a database solution for thermodynamic constant estimation}

    \author{%
        Moritz E. Beber\,$^{1,2,\dagger}$,
        Mattia G. Gollub\,$^{3}$,
        Dana Mozaffari\,$^{3,4}$,
        Kevin M. Shebek\,$^{5}$,
        Avi Flamholz\,$^{6}$,
        Ron Milo\,$^{7}$
        and Elad Noor\,$^{7,\dagger,}$%
        \footnote{To whom correspondence should be addressed.
            Tel: +972 8 934 4466; Email: elad.noor@weizmann.ac.il}
    }

    \address{%
        $^{1}$Novo Nordisk Foundation Center for Biosustainability, Technical University of Denmark, Kemitorvet, 2800 Kongens Lyngby, Denmark,
        $^{2}$Unseen Biometrics ApS, Fruebjergvej 3, 2100 København Ø, Denmark
        $^{3}$Department of Biosystems Science and Engineering and SIB Swiss Institute of Bioinformatics, ETH Z\"{u}rich, Basel, 4058, Switzerland
        $^{4}$Institute of Chemical Sciences and Engineering, EPFL, Lausanne, 1015, Switzerland
        $^{5}$Department of Chemical and Biological Engineering, Chemistry of Life Processes Institute, and Center for Synthetic Biology, Northwestern University, Evanston, Illinois 60208, USA
        $^{6}$Division of Biology and Biological Engineering, California Institute of Technology, Pasadena, CA 91125, USA
        $^{7}$Department of Plant and Environmental Sciences, Weizmann Institute of Science, Herzl 234, Rehovot 7610001, Israel
        $\dagger$These authors contributed equally
    }
    % Affiliation must include:
    % Department name, institution name, full road and district address,
    % state, Zip or postal code, country

    \history{%
        Received January 1, 2009;
        Revised February 1, 2009;
        Accepted March 1, 2009}

\maketitle


\begin{abstract}
eQuilibrator (\href{equilibrator.weizmann.ac.il}{equilibrator.weizmann.ac.il}) is a database of biochemical equilibrium constants and Gibbs free energies, originally designed as a web-based interface. While the website now counts around 1,000 distinct monthly users, its design could not accommodate larger compound databases and it lacked a scalable Application Programming Interface (API) for integration into other tools developed by the systems biology community. Here, we report on the recent updates to the database as well as the addition of a new Python-based interface to eQuilibrator that adds many new features such as a 100-fold larger compound database, the ability to add novel compounds, improvements in speed and memory use, and correction for \ce{Mg^2+} ion concentrations. Moreover, the new interface can compute the covariance matrix of the uncertainty between estimates, for which we show the advantages and describe the application in metabolic modeling. We foresee that these improvements will make thermodynamic modelling more accessible and facilitate the integration of eQuilibrator into other software platforms.
\end{abstract}


\section{Introduction}
The field of thermodynamics started in the midst of the industrial revolution as an effort to improve mechanical engines \cite{carnot_reflexions_1824}. The phenomenal success of the theory to describe the relationships between \textit{energy}, \textit{heat}, and \textit{work} and to provide accurate predictions of what is feasible, inspired countless other scientific endeavors, including molecular dynamics and even economics \cite{jinich_quantum_2014, jinich_quantum_2018, georgescu-roegen_entropy_1999}. Curiously, until recently, thermodynamic reasoning was relatively underutilized in metabolic modeling. We have identified four major reasons for this.
\textbf{The knowledge gap} - equilibrium constants for most biochemical reactions have not been measured.
\textbf{The computation gap} - thermodynamic constraints tend to make metabolic models more complicated. For example, Flux Balance Analysis (FBA) with thermodynamic constraints turns from a standard Linear Problem to an NP-complete Mixed-Integer one (MILP) \cite{henry_thermodynamics-based_2007,mahamkali_multitfa_2020}.
\textbf{The motivation gap} - it is not clear to everyone that using thermodynamics in models is actually necessary or even useful.
\textbf{The accessibility gap} - adding thermodynamics to an existing model is laborious. It involves tasks such as: mapping identifiers, adjusting the $\Delta G'^\circ$ values to the aqueous conditions, and annotating charged molecules correctly -- to name but a few.

One of the major breakthroughs in bridging the knowledge gap was suggested by Lydersen et al. \cite{lydersen_estimation_1955}, who adapted the \textit{group contribution} method (which can computationally predict molecular Gibbs energies) to organic chemistry. Decades later, Joback et al. \cite{joback_estimation_1987} and Mavrovouniotis et al. \cite{mavrovouniotis_group_1988} eventually implemented this idea for the context of biochemical reactions. This data-driven approach decomposes a compound into a list of predefined chemical groups, each of which is assumed to contribute a fixed amount to the compounds' Gibbs energy of formation. These group contributions are estimated by regression against measurements of reaction Gibbs energies, thus enabling estimation of thermodynamic potentials for the majority of reactions in central carbon metabolism \cite{feist_genome-scale_2007}.

\vspace{3cm}  % without this command, the text of the next paragraph is overlayed on the footer and cannot be read properly.

In the last decade, several improvements to the accuracy and coverage of this method have been proposed and implemented \cite{jankowski_group_2008, noor_integrated_2012, noor_consistent_2013, du_temperature_2018}. Nevertheless, some estimates cannot be performed using group contributions since certain molecular structures still do not have any experimental data and therefore reactions in which they appear are outside the scope of the methods. Furthermore, some structures seem to violate the assumption of independence between groups and tend to greatly increase the error of the estimates. In the end, although increasing accuracy by improving the method itself or by gathering more experimentally-derived equilibrium constants is still definitely worthwhile, one could argue that the current state-of-the-art has reached a sufficient level for many applications.

Similarly, the complexity gap has changed from a hard barrier to a minor inconvenience. Increasingly faster computers and powerful MILP solvers (such as IBM \href{https://www.ibm.com/products/ilog-cplex-optimization-studio}{CPLEX} and \href{https://www.gurobi.com/}{Gurobi} which offer free academic licenses) made it possible to solve large problems using personal computers, a task which was inconceivable only a decade ago.

The motivation gap is harder to overcome due to a chicken-and-egg problem. Since other technical issues delayed the application of thermodynamic models, it was difficult to demonstrate the usefulness of these models and therefore convince the scientific community that they are worth investing in. Nevertheless, several methods that take advantage of thermodynamic data already exist. Some exploit thermodynamic principles to constrain reaction directionality and metabolite concentrations \cite{holzhutter_principle_2004,henry_thermodynamics-based_2007,salvy_pytfa_2018}. Others use thermodynamic driving forces as a proxy for pathway efficiency \cite{noor_pathway_2014,hadicke_optmdfpathway:_2018}. More recently, probabilistic methods combining thermodynamic parameters have been suggested for parameter estimation \cite{lubitz_parameter_2019} and flux sampling \cite{gollub_probabilistic_2020}. These algorithms have the potential to improve the flux predictions produced by flux analysis \cite{noor_removing_2018, niebel_upper_2019, saldida_unbiased_2020}, and assist in the design of new metabolic pathways \cite{hadicke_optmdfpathway:_2018}. However, one of the toughest barriers computational approaches need to overcome is gaining the trust of experimental biologists, and it appears that a few cracks are forming already. For instance, thermodynamic driving forces were used to show how glycolisys is passively regulated \cite{park_near-equilibrium_2019}, and predictions based on thermodynamic enzyme efficiency \cite{flamholz_glycolytic_2013} have proven to fit quite well with experimental data from \textit{Z. mobilis} \cite{jacobson_metabolic_2019}.

\hl{It seems that the time has come to address the accessibility gap.} In recent years, a plethora of software tools have facilitated the reconstruction, validation, and analysis of genome-scale metabolic models and made them a community standard which is applied in thousands of scientific projects every year
\cite{carey_community_2020}. In 2012, we launched the first version of the eQuilibrator website, which aimed to broaden access to thermodynamic parameters and thermodynamic reasoning about metabolism \cite{flamholz_equilibratorbiochemical_2012}. eQuilibrator provides a simple search-focused interface for quickly finding the Gibbs energy change of a biochemical reaction, and is now used by $\sim$1,000 distinct users every month. However, eQuilibrator was designed to be used for single reactions. Therefore, it is inefficient at querying lists of reactions and doesn't account for correlations between multiple estimates. In this paper, we present a new Python package which is aimed at both novice and expert programmers that want to add thermodynamic parameters to their models.


\section{Results}

\hl{The Python package \texttt{equilibrator-api}, which is freely available on the Python Package Index and on conda-forge, significantly lowers the barrier to thermodynamic modeling in multiple use-cases. In fact, some of the thermodynamic analyses mentioned in the previous section have used an early version of this package to obtain the necessary thermodynamic constants. For example, a recent study} \cite{niebel_upper_2019} \hl{tested the hypothesis that an upper limit exists on the total Gibbs energy dissipation rate of cellular metabolism using estimates generated by \texttt{equilibrator-api}. Since this analysis required standard Gibbs energy estimates for thousands of reactions, this would have been untractable using the web interface. Genome-scale metabolic models with annotated metabolites, that use any of the standard chemical identifiers contained in MetaNetX, can make use of dedicated functions that provide a mapping to eQuilibrator compounds with thermodynamic information. These compounds can then be used by constraint-based and sampling frameworks, such as multiTFA} \cite{mahamkali_multitfa_2020} \hl{and Probabilistic Thermodynamic Analysis (PTA)} \cite{gollub_probabilistic_2020}. \hl{In the case of specific pathway models, such as ones designed for metabolic engineering and contain novel reactions, our framework provides additional tools for assessing the feasibility of the pathway} \cite{noor_pathway_2014,hadicke_optmdfpathway:_2018}.

\hl{Furthermore, since the launch of eQuilibrator in 2012} \cite{flamholz_equilibratorbiochemical_2012} \hl{we added a list of new features: a hundred times larger compound database, the ability to locally add new compounds to the database, calculating the full covariance matrix for the uncertainty between estimates, support for multi-compartment reactions, changing magnesium ion concentrations, tools for analyzing whole pathways, and general improvements in speed and memory use. Moreover, we now base all estimates on the component contribution method} \cite{noor_consistent_2013}, \hl{which required developing a way to calculate component contribution estimates for new reactions on-the-fly. The basis for these calculations is provided in the supplementary section} \ref{A-sec:on_the_fly}. \hl{Please note, that some of these features are not yet accessible through the online version of eQuilibrator, namely the larger database, adding new compounds, the covariance matrix, and multi-compartment reactions.}

\hl{Below, we expand on some of these new features, and the benefits they provide to our users.}

\subsection{Covariance matrix}
One of the advantages provided by the new system is the ability to compute the covariance matrix for the uncertainty of multiple estimates (a feature currently supported only in the Python package). In some cases, standard transformed formation ($\Delta_f G'^\circ$) or reaction ($\Delta_r G'^\circ$) energy estimates have large uncertainties when taken individually. However, uncertainties might be highly correlated, e.g., when reactions share a common compound or compounds share a common chemical group. Ignoring these correlations usually overestimates the uncertainty and may violate the first law of thermodynamics~\cite{noor_consistent_2013}. In contrast to per-reaction and per-compound uncertainties, the covariance matrix describes the joint uncertainty precisely.

\hl{The general formula for the standard Gibbs energy of a set of reactions, given their mean $\boldsymbol{\upmu}$ and covariance $\mathbf{\Sigma}$ is }
\begin{equation}
\Delta_r G'^\circ = \boldsymbol{\upmu} + \mathbf{\Sigma}^{1/2}\mathbf{m}\;,
\end{equation}
\hl{where $\mathbf{\Sigma}^{1/2}$ is a square root of $\mathbf{\Sigma}$. $\mathbf{m}$ can either be a standard multivariate normal random variable in case of random sampling applications or an optimization variable bounded within a desired confidence level for constraint-based approaches.}
\hl{Importantly, this formulation ensures that the computed Gibbs energies are consistent with the first law of thermodynamics, relieving the need for computationally more challenging constraints such as Gibbs energy balance}~\cite{niebel_upper_2019} (see supplementary section \ref{A-sec:multivariate} for more details).

\hl{This formulation of the uncertainty has already been available in a development version of eQulibrator for some time and was successfully applied in both statistical and constraint-based contexts: In PTA} \cite{gollub_probabilistic_2020} \hl{it is used to detect thermodynamic inaccuracies in metabolic models, to predict occurrences of substrate channeling, as well as to estimate metabolite concentrations and reaction fluxes with a sampling algorithm. Moreover, multiTFA} \cite{mahamkali_multitfa_2020} \hl{shows that accounting for covariance reduces the ranges of reaction energies also in constraint-based models.}

\subsection{Expanding the scope of compounds}

A frequent request from eQuilibrator users was adding compounds that are not present in the KEGG database \cite{kanehisa_kegg_2008}. We therefore modified eQuilibrator to use MetaNetX \cite{moretti_metanetxmnxref_2021}, a database that aggregates chemicals that are relevant for metabolic models from multiple online databases, including KEGG \cite{kanehisa_kegg_2008}, ChEBI \cite{hastings_chebi_2016}, BiGG\cite{king_bigg_2016}, ModelSEED\cite{henry_high-throughput_2010}, Swiss Lipids\cite{aimo_swisslipids_2015} (see Figure \ref{fig:eq3_design}). This expanded the repertoire from $\sim$10,000 to $\sim$1,000,000 compounds which can be accessed using identifiers from various namespaces. As a result, eQuilibrator can now be used with metabolic models from different sources (e.g., SEED or BiGG models) directly, without the need to map all compounds to KEGG identifiers in advance.

The eQuilibrator website was originally designed to provide pre-calculated Gibbs energy values for a large database of biochemical reactions. Advanced users often want to calculate \ce{\Delta G} values for molecules and reactions not found in the database (even after the expansion to MetaNetX). For example, metabolic pathway engineering often utilizes promiscuous enzymes to generate novel reactions, producing pathways with compounds not found in MetaNetX \cite{schwander_synthetic_2016,jeffryes_mines_2015,hadadi_atlas_2016,delepine_retropath20_2018,moriya_pathpred_2010}.

Previously, this would have required users to add their compounds to the database and rerun the component-contribution method -- a considerable effort. To reduce the amount of time it takes to estimate \ce{\Delta G} values of reactions with novel compounds we have extended the \texttt{equilibrator-assets} package, which is responsible for generating the compound database, to enable users to directly add new entries to a local version of the compound database (i.e. a \textit{local cache} as depicted in Figure \ref{fig:eq3_design}). The new compounds can be specified using the IUPAC International Chemical Identifier (InChI) or the Simplified Molecular Input Line Entry System (SMILES). These compounds can then be used directly with the \texttt{equilibrator-api} package, allowing for seamless integration of new compounds in thermodynamic analyses \cite{scheffen_new--nature_2021}. See the documentation at \url{https://equilibrator.readthedocs.io} for more details.

%% Moritz: For an eventual paper submission I feel that we could highlight the important role of the InChI (InChI-Key) and that models will fare best if their metabolites are annotated with that information.\\Elad: I agree. Can you do that, and perhaps also reference your new equilibrator-cheminfo package? We should also update the relevant text about MetaNetX (now after the update).

\subsection{Multi-compartment reactions}

The standard calculation for reaction Gibbs energies assumes that all reactants are in the same aqueous compartment, with a constant pH, pMg, ionic strength and temperature \cite{alberty_biochemical_1994}. However, most genome-scale metabolic models describe more than one compartment, usually separated by a lipid membrane, and contain many transport reactions that span compartments with different aqueous conditions. Furthermore, the membrane between each two compartments can be associated with an electrostatic potential $\Delta \Phi$ (in units of Volts) which affects the thermodynamics of charged ions traveling between them. When a reaction involves transport of metabolite species between compartments with different hydrogen ion activity or electric potential, we add the following term to its $\Delta_r G'^\circ$:
\begin{equation}
    -N_H \cdot RT\ln\left( 10^{\Delta \mathrm{pH}} \right) - Q \cdot F \Delta \Phi
\end{equation}
where $R$ is the gas constant, $T$ is the temperature, $F$ is Faraday's constant (the total electric charge of one mole of electrons -- $96.5$ kC mol$^{-1}$), $\Delta \mathrm{pH}$ is the difference in pH between initial and final compartment, $N_H$ is the net number of hydrogen ions transported from initial to final compartment, and $Q$ is the stoichiometric coefficient of the transported charges \cite{alberty_biochemical_1994, haraldsdottir_quantitative_2012}. Note that $RT\ln\left( 10^{\Delta pH} \right)$ is the energy difference between the compartments specifically for protons (due to their concentration gradient), and $F \Delta \Phi$ is the energy difference relevant to all charges (including protons).

In prior versions of eQuilibrator, adjusting the standard Gibbs energy of multi-compartment biochemical reactions, such as ones facilitated by membrane transporters, had to be performed as a post-processing step. For example, the \textit{vonBertalanffy} extension in the COBRA toolbox \cite{fleming_von_2011} performs such additional calculations as part of its pipeline. In eQuilibrator 3.0, we add functions to facilitate the adjustments required for multi-compartment reactions as part of our Python package. See supplementary section \ref{A-sec:mulicompartmental_example} for an example.

\subsection{Adjusting estimates to different pMg values}
Since ions are abundant in the cytosol and can bind metabolites to varying degrees, ion concentrations have a large effect on biochemical thermodynamics~\cite{alberty_biochemical_1994}. The concentration of protons (\ce{H+}), commonly expressed as the pH, is the most dramatic example of this phenomenon. However, this is not the only case. Magnesium ions (\ce{Mg^{2+}}) bind to many common biochemical moieties, especially phosphate, and have been shown to play a significant role in the thermodynamics of glycolysis~\cite{vojinovic_influence_2009}. For example, the dissociation constants for \ce{ATP} and \ce{ADP} are low enough for them to be in their complex forms \ce{Mg \cdot ATP^2-} and \ce{Mg \cdot ADP-} at a physiological intracellular pMg of 3 (which is defined similarly to pH, i.e. it corresponds to a concentration of $10^{-3}$ M -- or 1 mM -- of \ce{Mg^{2+}}).

Every compound can be seen as an ensemble of \textit{pseudoisomers}, molecules only differing in protonation or magnesium binding state. In a biochemical context, where all compounds are assumed to be in a buffered aqueous environment, we do not distinguish between pseudoisomers and refer to the entire ensemble as a \textit{metabolite}. Note that this assumption does not hold for transport reactions across membranes, which may selectively transport only specific pseudoisomers. For single compartment reactions it is thus convenient to discuss the standard \textit{transformed} Gibbs energy of formation $\Delta_f G'^\circ$ which groups together all pseudoisomers into one formation energy. It can be obtained using a Boltzmann-weighted mixture of its constituent pseudoisomers:
\begin{equation}
    \Delta_f G'^\circ ~=~ -RT\ln{\sum_j\mathrm{e}^{-\Delta_f G'^\circ(j)/(RT)}}\label{eq:legendre1}\,.
\end{equation}

The standard transformed Gibbs energies of formation $\Delta_f G'^\circ(j)$ for each pseudoisomer $j$ at given biochemical conditions can be calculated using the Legendre transformation
\begin{equation}
    \label{eq:legendre2}
    \Delta_f G'^\circ(j) ~=~ \Delta_f G^\circ(j) ~+~ \Theta_H(j) ~+~ \Theta_{Mg}(j)\,,
\end{equation}
where
\begin{align*}
    \Theta_H(j) &\equiv - N_H(j)[\Delta_f G^\circ(H^+) + RT\ln(10^{-pH})] \\
    \Theta_{Mg}(j) &\equiv - N_{Mg}(j)[\Delta_f G^\circ(Mg^{2+}) + RT\ln(10^{-pMg})]\,.
\end{align*}

The first term -- $\Delta_f G^\circ(j)$ -- is the \textit{chemical} standard Gibbs energy of formation of the pseudoisomer. The second term -- $\Theta_H(j)$ -- describes the contribution of protons to the Gibbs energy as a function of the pH. $N_H(j)$ is the number of protons associated to this pseudoisomer, and $\Delta_f G^\circ(H^+)$ is the standard formation energy of a proton, defined to be $0$ in this framework \cite{alberty_biochemical_1994}. Similarly, the effect of the concentration of Mg$^{2+}$ ions (quantified as pMg) can be taken into account by adding the term $\Theta_{Mg}(j)$. Similar to the case of protons, $N_{Mg}(j)$ is the number of Magnesium ions associated to this pseudoisomer, and $\Delta_f G^\circ(Mg^{2+})$ is the standard formation energy of Magnesium ions, equal to $-455.3$ kJ/mol.

Affinity to \ce{Mg^2+} varies between compounds and pseudoisomers. The presence of certain chemical moieties, such as phosphate groups, tends to increase the binding affinity \cite{alberty_thermodynamics_2003}, while increasing the protonation state tends to decrease the affinity. Unfortunately, the availability of affinity constants for \ce{Mg^2+} is much lower than for \ce{H^+}. In eQuilibrator, we used $\Delta_f G'^\circ(j)$ for magnesium-bound pseudoisomers collected by Vojinovi\'{c} and von Stockar \cite{vojinovic_influence_2009} and affinities predicted by Du et al. \cite{du_temperature_2018}. For all other pseudoisomers, we assumed that their affinity is weak and has negligible effect on thermodynamics.

After populating the database with magnesium-bound pseudoisomers, we computed the root mean square error (RMSE) for all reactions from the NIST TECR database \cite{goldberg_thermodynamics_2004}. When taking magnesium into account, the RMSE improved slightly from 2.99 to 2.93.

In the online version of eQuilibrator, pMg can be adjusted in the same way as pH and ionic strength, by entering the value in the proper text field. Similarly, the Python package contains a new \texttt{p\_mg} variable that can be set directly, along with \texttt{p\_h} and \texttt{ionic\_strength}, as explained in the documentation at \url{https://equilibrator.readthedocs.io}.

\subsection{Upgrades to the eQuilibrator website}
Compared to its original version \cite{flamholz_equilibratorbiochemical_2012}, the eQuilibrator website has undergone several major updates. As mentioned earlier, the backend method for estimating reaction Gibbs energies was changed to component contributions \cite{noor_consistent_2013}. This method provides a consistent set of reaction energies, without compromising the accuracy of reactions and compounds with direct experimental data. More recently, we added the ability to adjust the pMg as well as an option to plot $\Delta G'$ as a function of pMg. A more comprehensive list of changes can be found on the website itself at \url{https://equilibrator.weizmann.ac.il/static/classic_rxns/updates.html}.

Besides improvements to the core search function, the website now offers tools for pathway analysis based on two methods: Max-min Driving Force (MDF) \cite{noor_pathway_2014} and Enzyme Cost Minimization \cite{noor_protein_2016}. \hl{Both these methods aim to provide a quantitative scoring system that can be used to rank natural or engineered metabolic pathways -- either to assist in the design process for metabolic engineers, or for understanding the design principles that govern the evolution of metabolic pathways. MDF is a purely thermodynamic-based approach that assumes driving forces are maximized within the constraints imposed by metabolite concentration bounds and the necessary coupling between adjacent reactions. ECM uses a convex optimization approach to find the optimal distribution of enzyme and metabolite concentrations, where the pathway flux is maximized.} Both algorithms are also available in the Python package named \textit{equilibrator-pathway}.



\subsection{Complete code refactoring}
In order to extend the eQuilibrator framework to new use-cases (such as those described above) a complete code refactoring was required -- creating separate packages for each distinct function. The original code was designed exclusively for a single workflow: starting with analyzing the chemical structures (group decompositions), reverse-transforming the measured equilibrium constants to chemical Gibbs energies \cite{alberty_legendre_1997}, solving the linear regression problems to find the group contribution energies, and using the solutions to estimate formation energies for all compounds in the KEGG database. This long procedure was not useful for users who only wanted to apply component contributions on a list of their own reactions.

Therefore, we have redesigned the entire component-contribution package, moved it to a new \href{https://gitlab.com/equilibrator/component-contribution}{Git repository}, and integrated it completely into a larger framework denoted \textit{eQuilibrator 3.0 suite} (see Figure \ref{fig:eq3_design}). In addition, we raised the coding standards, e.g. by running automated tools for coverage, unit-testing, and documentation (available on \url{equilibrator.readthedocs.io}). We also facilitated the installation of the packages by submitting them to the Python Package Index (\url{https://pypi.org}) and conda-forge (\url{https://conda-forge.org}).

\hl{Following this refactoring, Python users can very easily install the equilibrator packages on their own computer using a simple command (with full support for Windows OS, MacOS, and Linux). The improved documentation and software stability facilitate this further, even for novice programmers. Furthermore, proper versioning enables researchers to easily trace back changes and reproduce their results generated by older versions of the software.}

\begin{figure*}[t!]
    \centering
    \includegraphics[width=\textwidth]{figure1.pdf}
    \caption{The design of the eQuilibrator 3.0 suite. At the core, the equilibrator-cache Python package defines and manages the database of all compounds, denoted \textit{equilibrator/cache}. All compound identifiers and names come from MetaNetX, which aggregates several popular compound repositories. The component-contribution package is responsible for handling training data, group decomposition, Legendre transforms (for pH, pMg and ionic strength adjustments), and the final estimation of Gibbs energies for new compounds and reactions. Some scripts required for rebuilding the database and adding new compounds to a local database are stored in equilibrator-assets. All data relevant for running the eQuilibrator packages is stored in Zenodo and is freely available. This includes the experimental $\rm K_{eq}$ data used to train the component contribution method, which comes mainly from NIST TECR database \cite{goldberg_thermodynamics_2004}. The equilibrator-api package exposes nearly all functions relevant for users with sufficient programming skills, and is required for running eQuilibrator in batch mode (e.g. on an entire metabolic model). \hl{Some advanced features (namely pathway profiling using Max-min Driving Force analysis} \cite{noor_pathway_2014} \hl{or Enzyme Cost Minimization} \cite{noor_protein_2016}) \hl{are available through a separate package called equilibrator-pahtway}. For non-programmers, the web interface provides quick and easy access to eQuilibrator estimates, but is only designed to deal with a single reaction at a time.}
    \label{fig:eq3_design}
\end{figure*}

\section{Discussion}

The eQuilibrator 3.0 suite marks a major shift in the focus of our software which has so far been mainly geared for single reaction searches or small biochemical networks (pathways) and exposed via a web interface. Now, we reach out to a larger audience, including modelers who want to populate genome-scale metabolic networks with thermodynamic parameters as well as metabolic engineers who want to scan a large set of parameters for their designs. In addition, the software package can now be much more easily integrated into other Python-based frameworks and pipelines such as COBRApy \cite{ebrahim_cobrapy_2013,lloyd_cobrame:_2018}, MEMOTE \cite{lieven_memote_2020}, ModelSEED \cite{seaver_modelseed_2020}, and CarveMe \cite{machado_fast_2018}. Furthermore, in supplementary section \ref{A-sec:compressed}, we show how one can efficiently store a set of pre-calculated matrices that can be used to calculate the final estimates (including full uncertainty matrices). This can facilitate building compound databases for frameworks that are not based on Python, or that rely on mathematical calculations that are not yet supported within the eQuilibrator codebase.

The major improvements that we introduce in this work are: (1) an API supported by a refactored codebase that is much more suited to modeling applications and integration into other software, (2) improvements in speed and memory use, (3) correction for \ce{Mg^2+} ions, (4) multi-compartment reactions, (5) access to the full covariance matrix for uncertainty modeling, (6) cross-databases identification of molecules and reactions with a much larger pool of compounds (provided by MetaNetX) and the ability to add novel compounds. We continue to support community-driven development and open source standards, by publishing all the code under the permissive MIT license and making it available on \href{https://gitlab.com/equilibrator}{GitLab}. All other raw data needed for the algorithm is licensed under a \href{https://creativecommons.org/licenses/by/4.0/legalcode}{Creative Commons 4.0} license, and stored on \url{www.zenodo.org}.

We are open to suggestions for what could be added to eQuilibrator in the future via discussions on \href{https://gitlab.com/equilibrator/equilibrator-api/-/issues}{\textit{GitLab issues}} and we welcome contributions from the community. For example, new features already under consideration include temperature adjustment based on separate entropy/enthalpy estimates, a fully automated script for populating \href{http://sbml.org}{SBML} (Systems Biology Markup Language) models with thermodynamic parameters (including multi-compartment reactions), and integration with common platforms and use-cases such as support for Thermodynamic-based Flux Analysis \cite{salvy_pytfa_2018} in COBRApy.

We believe that eQuilibrator 3.0 is a substantial step forward in closing the accessibility gap and hope that together with other recent advances \cite{noor_pathway_2014,noor_protein_2016,salvy_pytfa_2018,gollub_probabilistic_2020,lubitz_parameter_2019} will bring forth the golden age of thermodynamics in the field of metabolic modeling.

\section{Data Availability}
The source code for all the Python packages mentioned in this manuscript can be found at \url{https://gitlab.com/equilibrator}. All relevant data and models are stored on Zenodo, and are linked to from the repository. The documentation can be found at \url{https://equilibrator.readthedocs.io}.

The eQuilibrator suite depends on several open-source packages, such as: \href{http://openbabel.org/}{OpenBabel} \cite{oboyle_pybel_2008}, \href{https://rdkit.org/}{RDKit}, \href{https://numpy.org/}{NumPy}, \href{https://www.scipy.org/}{SciPy}, \href{https://pandas.pydata.org/}{Pandas}, \href{https://pint.readthedocs.io/en/stable/}{Pint}, and \href{https://www.sqlalchemy.org/}{SQLAlchemy}.

In addition, estimating acid-base dissociation constants was done using Marvin Calculator, Calculator version 21.13.0, ChemAxon (\url{www.chemaxon.com}), under an academic license. Marvin Calculator is also required for adding new compounds to a local database.

\section*{Funding}
M.G.G. was supported by the Swiss National Science Foundation Sinergia project \#177164. M.E.B. was partly supported by Horizon
2020 - Research and Innovation Framework Programme grant 686070 (DD-DeCaF). K.M.S. was supported by the U.S. Department of Energy, Office of Science, Office of Biological and Environmental Research under Award Number DE-SC0018249.

\section*{Acknowledgments}
Dedicated to the memory of Arren Bar-Even (1980-2020).
We thank Jörg Stelling for a helpful discussion on the effect of magnesium ions.

\textit{Conflict of interest statement.} None declared.

\bibliography{references}
\bibliographystyle{plain}
%\printbibliography[title={References}]

\end{document}

